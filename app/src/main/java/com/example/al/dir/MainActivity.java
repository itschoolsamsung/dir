package com.example.al.dir;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.widget.TextView;
import java.io.File;

public class MainActivity extends Activity {

	private final StringBuilder dirStr = new StringBuilder();
	private TextView dirView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		dirView = findViewById(R.id.dir);
		new AsyncRecursion().execute();
	}

	private class AsyncRecursion extends AsyncTask {
		private void recursiveCall(String path, String indent) {
			Log.i("_Q_", path);
			File[] fileList = new File(path).listFiles(); //список фалов и директорий внутри указанной директории

			if (fileList != null) {
				for (File file : fileList) { //обходим список
					if (file.isDirectory()) { //если директория, сохраняем имя и рекурсивно повторяем поиск внутри неё
						dirStr.append(indent).append(" /").append(file.getName()).append("\n");
						recursiveCall(file.getAbsolutePath(), indent + "\t\t");
					} else { //иначе просто сохраняем имя файла
						dirStr.append(indent).append(" ").append(file.getName()).append("\n");
					}
				}
			}
		}

		@Override
		protected Object doInBackground(Object[] objects) { //Метод выполняется в отдельном потоке, нет доступа к UI
			recursiveCall(Environment.getRootDirectory().getPath(), "");

			return null;
		}

		@Override
		protected void onPostExecute(Object o) { //Метод выполняется после doInBackground, есть доступ к UI
			super.onPostExecute(o);

			dirView.setText(dirStr);
		}
	}
}
